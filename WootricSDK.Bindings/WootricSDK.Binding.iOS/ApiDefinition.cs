﻿using System;
using Foundation;
using UIKit;
//using WootricSDK;

namespace WootricSDK 
{
    // @interface Wootric : NSObject
    [BaseType(typeof(NSObject))]
    interface Wootric
    {
        // +(void)configureWithClientID:(NSString *)clientID clientSecret:(NSString *)clientSecret accountToken:(NSString *)accountToken;
        [Static]
        [Export("configureWithClientID:clientSecret:accountToken:")]
        void ConfigureWithClientID(string clientID, string clientSecret, string accountToken);

        // +(void)configureWithClientID:(NSString *)clientID accountToken:(NSString *)accountToken;
        [Static]
        [Export("configureWithClientID:accountToken:")]
        void ConfigureWithClientID(string clientID, string accountToken);

        // +(void)showSurveyInViewController:(UIViewController *)viewController;
        [Static]
        [Export("showSurveyInViewController:")]
        void ShowSurveyInViewController(UIViewController viewController);

        // +(void)setEndUserCreatedAt:(NSNumber *)externalCreatedAt;
        [Static]
        [Export("setEndUserCreatedAt:")]
        void SetEndUserCreatedAt(NSNumber externalCreatedAt);

        // +(void)setEndUserEmail:(NSString *)endUserEmail;
        [Static]
        [Export("setEndUserEmail:")]
        void SetEndUserEmail(string endUserEmail);

        // +(void)setEndUserExternalId:(NSString *)externalId;
        [Static]
        [Export("setEndUserExternalId:")]
        void SetEndUserExternalId(string externalId);

        // +(void)setEndUserPhoneNumber:(NSString *)phoneNumber;
        [Static]
        [Export("setEndUserPhoneNumber:")]
        void SetEndUserPhoneNumber(string phoneNumber);

        // +(void)setProductNameForEndUser:(NSString *)productName;
        [Static]
        [Export("setProductNameForEndUser:")]
        void SetProductNameForEndUser(string productName);

        // +(void)setCustomLanguage:(NSString *)languageCode;
        [Static]
        [Export("setCustomLanguage:")]
        void SetCustomLanguage(string languageCode);

        // +(void)setCustomAudience:(NSString *)audience;
        [Static]
        [Export("setCustomAudience:")]
        void SetCustomAudience(string audience);

        // +(void)setCustomProductName:(NSString *)productName;
        [Static]
        [Export("setCustomProductName:")]
        void SetCustomProductName(string productName);

        // +(void)setCustomFinalThankYou:(NSString *)finalThankYou;
        [Static]
        [Export("setCustomFinalThankYou:")]
        void SetCustomFinalThankYou(string finalThankYou);

        // +(void)setCustomQuestion:(NSString *)question;
        [Static]
        [Export("setCustomQuestion:")]
        void SetCustomQuestion(string question);

        // +(NSDictionary *)endUserProperties;
        // +(void)setEndUserProperties:(NSDictionary *)customProperties;
        [Static]
        [Export("endUserProperties")]
        NSDictionary EndUserProperties { get; set; }

        // +(void)setFirstSurveyAfter:(NSNumber *)firstSurveyAfter;
        [Static]
        [Export("setFirstSurveyAfter:")]
        void SetFirstSurveyAfter(NSNumber firstSurveyAfter);

        // +(void)setSurveyedDefault:(BOOL)flag;
        [Static]
        [Export("setSurveyedDefault:")]
        void SetSurveyedDefault(bool flag);

        // +(void)surveyImmediately:(BOOL)flag;
        [Static]
        [Export("surveyImmediately:")]
        void SurveyImmediately(bool flag);

        // +(void)forceSurvey:(BOOL)flag;
        [Static]
        [Export("forceSurvey:")]
        void ForceSurvey(bool flag);

        // +(void)skipFeedbackScreenForPromoter:(BOOL)flag;
        [Static]
        [Export("skipFeedbackScreenForPromoter:")]
        void SkipFeedbackScreenForPromoter(bool flag);

        // +(void)passScoreAndTextToURL:(BOOL)flag;
        [Static]
        [Export("passScoreAndTextToURL:")]
        void PassScoreAndTextToURL(bool flag);

        // +(void)setFacebookPage:(NSURL *)facebookPage;
        [Static]
        [Export("setFacebookPage:")]
        void SetFacebookPage(NSUrl facebookPage);

        // +(void)setTwitterHandler:(NSString *)twitterHandler;
        [Static]
        [Export("setTwitterHandler:")]
        void SetTwitterHandler(string twitterHandler);

        // +(void)setThankYouMessage:(NSString *)thankYouMessage;
        [Static]
        [Export("setThankYouMessage:")]
        void SetThankYouMessage(string thankYouMessage);

        // +(void)setDetractorThankYouMessage:(NSString *)detractorThankYouMessage;
        [Static]
        [Export("setDetractorThankYouMessage:")]
        void SetDetractorThankYouMessage(string detractorThankYouMessage);

        // +(void)setPassiveThankYouMessage:(NSString *)passiveThankYouMessage;
        [Static]
        [Export("setPassiveThankYouMessage:")]
        void SetPassiveThankYouMessage(string passiveThankYouMessage);

        // +(void)setPromoterThankYouMessage:(NSString *)promoterThankYouMessage;
        [Static]
        [Export("setPromoterThankYouMessage:")]
        void SetPromoterThankYouMessage(string promoterThankYouMessage);

        // +(void)setThankYouLinkWithText:(NSString *)thankYouLinkText URL:(NSURL *)thankYouLinkURL;
        [Static]
        [Export("setThankYouLinkWithText:URL:")]
        void SetThankYouLinkWithText(string thankYouLinkText, NSUrl thankYouLinkURL);

        // +(void)setDetractorThankYouLinkWithText:(NSString *)detractorThankYouLinkText URL:(NSURL *)detractorThankYouLinkURL;
        [Static]
        [Export("setDetractorThankYouLinkWithText:URL:")]
        void SetDetractorThankYouLinkWithText(string detractorThankYouLinkText, NSUrl detractorThankYouLinkURL);

        // +(void)setPassiveThankYouLinkWithText:(NSString *)passiveThankYouLinkText URL:(NSURL *)passiveThankYouLinkURL;
        [Static]
        [Export("setPassiveThankYouLinkWithText:URL:")]
        void SetPassiveThankYouLinkWithText(string passiveThankYouLinkText, NSUrl passiveThankYouLinkURL);

        // +(void)setPromoterThankYouLinkWithText:(NSString *)promoterThankYouLinkText URL:(NSURL *)promoterThankYouLinkURL;
        [Static]
        [Export("setPromoterThankYouLinkWithText:URL:")]
        void SetPromoterThankYouLinkWithText(string promoterThankYouLinkText, NSUrl promoterThankYouLinkURL);

        // +(void)setCustomFollowupPlaceholderForPromoter:(NSString *)promoterPlaceholder passive:(NSString *)passivePlaceholder detractor:(NSString *)detractorPlaceholder;
        [Static]
        [Export("setCustomFollowupPlaceholderForPromoter:passive:detractor:")]
        void SetCustomFollowupPlaceholderForPromoter(string promoterPlaceholder, string passivePlaceholder, string detractorPlaceholder);

        // +(void)setCustomFollowupQuestionForPromoter:(NSString *)promoterQuestion passive:(NSString *)passiveQuestion detractor:(NSString *)detractorQuestion;
        [Static]
        [Export("setCustomFollowupQuestionForPromoter:passive:detractor:")]
        void SetCustomFollowupQuestionForPromoter(string promoterQuestion, string passiveQuestion, string detractorQuestion);

        // +(void)setCustomValueForResurveyThrottle:(NSNumber *)resurveyThrottle visitorPercentage:(NSNumber *)visitorPercentage registeredPercentage:(NSNumber *)registeredPercentage dailyResponseCap:(NSNumber *)dailyResponseCap;
        [Static]
        [Export("setCustomValueForResurveyThrottle:visitorPercentage:registeredPercentage:dailyResponseCap:")]
        void SetCustomValueForResurveyThrottle(NSNumber resurveyThrottle, NSNumber visitorPercentage, NSNumber registeredPercentage, NSNumber dailyResponseCap);

        // +(void)setCustomTimeDelay:(NSInteger)customTimeDelay;
        [Static]
        [Export("setCustomTimeDelay:")]
        void SetCustomTimeDelay(nint customTimeDelay);

        // +(void)setSendButtonBackgroundColor:(UIColor *)color;
        [Static]
        [Export("setSendButtonBackgroundColor:")]
        void SetSendButtonBackgroundColor(UIColor color);

        // +(void)setSliderColor:(UIColor *)color;
        [Static]
        [Export("setSliderColor:")]
        void SetSliderColor(UIColor color);

        // +(void)setThankYouButtonBackgroundColor:(UIColor *)color;
        [Static]
        [Export("setThankYouButtonBackgroundColor:")]
        void SetThankYouButtonBackgroundColor(UIColor color);

        // +(void)setSocialSharingColor:(UIColor *)color;
        [Static]
        [Export("setSocialSharingColor:")]
        void SetSocialSharingColor(UIColor color);

        // +(void)setLogLevelNone;
        [Static]
        [Export("setLogLevelNone")]
        void SetLogLevelNone();

        // +(void)setLogLevelError;
        [Static]
        [Export("setLogLevelError")]
        void SetLogLevelError();

        // +(void)setLogLevelVerbose;
        [Static]
        [Export("setLogLevelVerbose")]
        void SetLogLevelVerbose();

        // +(void)setSurveyTypeScale:(NSInteger)surveyTypeScale;
        [Static]
        [Export("setSurveyTypeScale:")]
        void SetSurveyTypeScale(nint surveyTypeScale);

        // +(void)showOptOut:(BOOL)flag;
        [Static]
        [Export("showOptOut:")]
        void ShowOptOut(bool flag);

        // +(NSNotificationName)surveyWillAppearNotification;
        [Static]
        [Export("surveyWillAppearNotification")]
        string SurveyWillAppearNotification { get; }

        // +(NSNotificationName)surveyWillDisappearNotification;
        [Static]
        [Export("surveyWillDisappearNotification")]
        string SurveyWillDisappearNotification { get; }

        // +(NSNotificationName)surveyDidAppearNotification;
        [Static]
        [Export("surveyDidAppearNotification")]
        string SurveyDidAppearNotification { get; }

        // +(NSNotificationName)surveyDidDisappearNotification;
        [Static]
        [Export("surveyDidDisappearNotification")]
        string SurveyDidDisappearNotification { get; }

        // +(id)delegate;
        // +(void)setDelegate:(id)delegate;
        [Static]
        [Export("delegate")]
        NSObject Delegate { get; set; }
    }

    // @interface SEGWootric : NSObject
    [BaseType(typeof(NSObject))]
    interface SEGWootric
    {
        // -(void)configureWithClientID:(NSString *)clientID clientSecret:(NSString *)clientSecret accountToken:(NSString *)accountToken;
        [Export("configureWithClientID:clientSecret:accountToken:")]
        void ConfigureWithClientID(string clientID, string clientSecret, string accountToken);

        // -(void)showSurveyInViewController:(UIViewController *)viewController;
        [Export("showSurveyInViewController:")]
        void ShowSurveyInViewController(UIViewController viewController);

        // -(void)setEndUserCreatedAt:(NSNumber *)externalCreatedAt;
        [Export("setEndUserCreatedAt:")]
        void SetEndUserCreatedAt(NSNumber externalCreatedAt);

        // -(void)setEndUserEmail:(NSString *)endUserEmail;
        [Export("setEndUserEmail:")]
        void SetEndUserEmail(string endUserEmail);

        // -(void)setProductNameForEndUser:(NSString *)productName;
        [Export("setProductNameForEndUser:")]
        void SetProductNameForEndUser(string productName);

        // -(void)setCustomLanguage:(NSString *)languageCode;
        [Export("setCustomLanguage:")]
        void SetCustomLanguage(string languageCode);

        // -(void)setCustomAudience:(NSString *)audience;
        [Export("setCustomAudience:")]
        void SetCustomAudience(string audience);

        // -(void)setCustomProductName:(NSString *)productName;
        [Export("setCustomProductName:")]
        void SetCustomProductName(string productName);

        // -(void)setCustomFinalThankYou:(NSString *)finalThankYou;
        [Export("setCustomFinalThankYou:")]
        void SetCustomFinalThankYou(string finalThankYou);

        // -(void)setCustomQuestion:(NSString *)question;
        [Export("setCustomQuestion:")]
        void SetCustomQuestion(string question);

        // -(NSDictionary *)endUserProperties;
        // -(void)setEndUserProperties:(NSDictionary *)customProperties;
        [Export("endUserProperties")]
        NSDictionary EndUserProperties { get; set; }

        // -(void)setFirstSurveyAfter:(NSNumber *)firstSurveyAfter;
        [Export("setFirstSurveyAfter:")]
        void SetFirstSurveyAfter(NSNumber firstSurveyAfter);

        // -(void)setSurveyedDefault:(BOOL)flag;
        [Export("setSurveyedDefault:")]
        void SetSurveyedDefault(bool flag);

        // -(void)surveyImmediately:(BOOL)flag;
        [Export("surveyImmediately:")]
        void SurveyImmediately(bool flag);

        // -(void)forceSurvey:(BOOL)flag;
        [Export("forceSurvey:")]
        void ForceSurvey(bool flag);

        // -(void)skipFeedbackScreenForPromoter:(BOOL)flag;
        [Export("skipFeedbackScreenForPromoter:")]
        void SkipFeedbackScreenForPromoter(bool flag);

        // -(void)passScoreAndTextToURL:(BOOL)flag;
        [Export("passScoreAndTextToURL:")]
        void PassScoreAndTextToURL(bool flag);

        // -(void)setFacebookPage:(NSURL *)facebookPage;
        [Export("setFacebookPage:")]
        void SetFacebookPage(NSUrl facebookPage);

        // -(void)setTwitterHandler:(NSString *)twitterHandler;
        [Export("setTwitterHandler:")]
        void SetTwitterHandler(string twitterHandler);

        // -(void)setThankYouMessage:(NSString *)thankYouMessage;
        [Export("setThankYouMessage:")]
        void SetThankYouMessage(string thankYouMessage);

        // -(void)setDetractorThankYouMessage:(NSString *)detractorThankYouMessage;
        [Export("setDetractorThankYouMessage:")]
        void SetDetractorThankYouMessage(string detractorThankYouMessage);

        // -(void)setPassiveThankYouMessage:(NSString *)passiveThankYouMessage;
        [Export("setPassiveThankYouMessage:")]
        void SetPassiveThankYouMessage(string passiveThankYouMessage);

        // -(void)setPromoterThankYouMessage:(NSString *)promoterThankYouMessage;
        [Export("setPromoterThankYouMessage:")]
        void SetPromoterThankYouMessage(string promoterThankYouMessage);

        // -(void)setThankYouLinkWithText:(NSString *)thankYouLinkText URL:(NSURL *)thankYouLinkURL;
        [Export("setThankYouLinkWithText:URL:")]
        void SetThankYouLinkWithText(string thankYouLinkText, NSUrl thankYouLinkURL);

        // -(void)setDetractorThankYouLinkWithText:(NSString *)detractorThankYouLinkText URL:(NSURL *)detractorThankYouLinkURL;
        [Export("setDetractorThankYouLinkWithText:URL:")]
        void SetDetractorThankYouLinkWithText(string detractorThankYouLinkText, NSUrl detractorThankYouLinkURL);

        // -(void)setPassiveThankYouLinkWithText:(NSString *)passiveThankYouLinkText URL:(NSURL *)passiveThankYouLinkURL;
        [Export("setPassiveThankYouLinkWithText:URL:")]
        void SetPassiveThankYouLinkWithText(string passiveThankYouLinkText, NSUrl passiveThankYouLinkURL);

        // -(void)setPromoterThankYouLinkWithText:(NSString *)promoterThankYouLinkText URL:(NSURL *)promoterThankYouLinkURL;
        [Export("setPromoterThankYouLinkWithText:URL:")]
        void SetPromoterThankYouLinkWithText(string promoterThankYouLinkText, NSUrl promoterThankYouLinkURL);

        // -(void)setCustomFollowupPlaceholderForPromoter:(NSString *)promoterPlaceholder passive:(NSString *)passivePlaceholder detractor:(NSString *)detractorPlaceholder;
        [Export("setCustomFollowupPlaceholderForPromoter:passive:detractor:")]
        void SetCustomFollowupPlaceholderForPromoter(string promoterPlaceholder, string passivePlaceholder, string detractorPlaceholder);

        // -(void)setCustomFollowupQuestionForPromoter:(NSString *)promoterQuestion passive:(NSString *)passiveQuestion detractor:(NSString *)detractorQuestion;
        [Export("setCustomFollowupQuestionForPromoter:passive:detractor:")]
        void SetCustomFollowupQuestionForPromoter(string promoterQuestion, string passiveQuestion, string detractorQuestion);

        // -(void)setCustomValueForResurveyThrottle:(NSNumber *)resurveyThrottle visitorPercentage:(NSNumber *)visitorPercentage registeredPercentage:(NSNumber *)registeredPercentage dailyResponseCap:(NSNumber *)dailyResponseCap;
        [Export("setCustomValueForResurveyThrottle:visitorPercentage:registeredPercentage:dailyResponseCap:")]
        void SetCustomValueForResurveyThrottle(NSNumber resurveyThrottle, NSNumber visitorPercentage, NSNumber registeredPercentage, NSNumber dailyResponseCap);

        // -(void)setSendButtonBackgroundColor:(UIColor *)color;
        [Export("setSendButtonBackgroundColor:")]
        void SetSendButtonBackgroundColor(UIColor color);

        // -(void)setSliderColor:(UIColor *)color;
        [Export("setSliderColor:")]
        void SetSliderColor(UIColor color);

        // -(void)setThankYouButtonBackgroundColor:(UIColor *)color;
        [Export("setThankYouButtonBackgroundColor:")]
        void SetThankYouButtonBackgroundColor(UIColor color);

        // -(void)setSocialSharingColor:(UIColor *)color;
        [Export("setSocialSharingColor:")]
        void SetSocialSharingColor(UIColor color);

        // -(void)setLogLevelNone;
        [Export("setLogLevelNone")]
        void SetLogLevelNone();

        // -(void)setLogLevelError;
        [Export("setLogLevelError")]
        void SetLogLevelError();

        // -(void)setLogLevelVerbose;
        [Export("setLogLevelVerbose")]
        void SetLogLevelVerbose();

        // -(void)showOptOut:(BOOL)flag;
        [Export("showOptOut:")]
        void ShowOptOut(bool flag);
    }

    // @interface WTRLogger : NSObject
    [BaseType(typeof(NSObject))]
    interface WTRLogger
    {
        //// +(WTRLogLevel)logLevel;
        //// +(void)setLogLevel:(WTRLogLevel)level;
        //[Static]
        //[Export("logLevel")]
        //WTRLogLevel LogLevel { get; set; }

        // +(void)log:(NSString *)format, ... __attribute__((format(NSString, 1, 2)));
        [Static, Internal]
        [Export("log:", IsVariadic = true)]
        void Log(string format, IntPtr varArgs);

        //// +(void)log:(NSString *)format arguments:(va_list)argList __attribute__((format(NSString, 1, 0)));
        //[Static]
        //[Export("log:arguments:")]
        //unsafe void Log(string format, sbyte* argList);

        // +(void)logLevel:(WTRLogLevel)level format:(NSString *)format, ... __attribute__((format(NSString, 2, 3)));
        [Static, Internal]
        [Export("logLevel:format:", IsVariadic = true)]
        void LogLevel(WTRLogLevel level, string format, IntPtr varArgs);

        //// +(void)logLevel:(WTRLogLevel)level format:(NSString *)format arguments:(va_list)argList __attribute__((format(NSString, 2, 0)));
        //[Static]
        //[Export("logLevel:format:arguments:")]
        //unsafe void LogLevel(WTRLogLevel level, string format, sbyte* argList);

        // +(void)logLevel:(WTRLogLevel)level message:(NSString *)message;
        [Static]
        [Export("logLevel:message:")]
        void LogLevel(WTRLogLevel level, string message);

        // +(void)logError:(NSString *)format, ... __attribute__((format(NSString, 1, 2)));
        [Static, Internal]
        [Export("logError:", IsVariadic = true)]
        void LogError(string format, IntPtr varArgs);

        //// +(void)logError:(NSString *)format arguments:(va_list)argList __attribute__((format(NSString, 1, 0)));
        //[Static]
        //[Export("logError:arguments:")]
        //unsafe void LogError(string format, sbyte* argList);
    }

    // @protocol WTRSurveyDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface WTRSurveyDelegate
    {
        // @required -(void)willPresentSurvey;
        [Abstract]
        [Export("willPresentSurvey")]
        void WillPresentSurvey();

        // @required -(void)didPresentSurvey;
        [Abstract]
        [Export("didPresentSurvey")]
        void DidPresentSurvey();

        // @required -(void)willHideSurvey;
        [Abstract]
        [Export("willHideSurvey")]
        void WillHideSurvey();

        // @required -(void)didHideSurvey:(NSDictionary *)data;
        [Abstract]
        [Export("didHideSurvey:")]
        void DidHideSurvey(NSDictionary data);
    }

    [Static]
    partial interface Constants
    {
        // extern double WootricSDKMainVersionNumber;
        [Field("WootricSDKMainVersionNumber", "__Internal")]
        double WootricSDKMainVersionNumber { get; }

        // extern const unsigned char [] WootricSDKMainVersionString;
        [Field("WootricSDKMainVersionString", "__Internal")]
        IntPtr WootricSDKMainVersionString { get; }
        //byte[] WootricSDKMainVersionString { get; }
    }

}