﻿using System;
using ObjCRuntime;

namespace WootricSDK
{
    [Native]
    public enum WTRLogLevel : long
    {
        None,
        Error,
        Verbose
    }

}